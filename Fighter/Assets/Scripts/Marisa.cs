using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Marisa : MonoBehaviour
{
    //rb vars
    private float spd = 3.5f;
    private Rigidbody2D rb;
    private Transform colision;

    //gameplay vars
    private float maxhp = 240;
    private float hp = 240;

    private float dmgval = 20;

    //state machine changer cuando esta en doingaction
    private int stanum = 0;

    //movement vars
    private float xmove = 0f;
    private float ymove = 0f;
    private bool jump = false;
    private bool canjump = false;
    public float jumpforce = 400f;

    //effectors functionality
    private bool plateffector = false;
    private bool conveyed = false;

    //cant move when doing action
    private bool DoingAction = false;

    //shield
    private bool shield = false;
    private SpriteRenderer shieldSprite;

    //oponent
    public GameObject adversary;

    public Animator ani;

    //attack colliders and vars
    public GameObject farhit;
    public GameObject midhit;
    public GameObject closehit;
    private int hitcounter = 0;
    private bool hitbool = false;

    public GameObject projectileprefab;

    int lightcomN = 0;
    int magiccomN = 0;
    int magiccomN2 = 0;

    public GameObject bloodemitter;



    private scr_Manager manager;

    //local scale
    Vector3 lolo;
    Vector3 lolostatic;
    private bool scalerX = false;

    //init
    void Start()
    {
        shieldSprite = gameObject.transform.GetChild(2).GetComponent<SpriteRenderer>();
        manager = GameObject.FindGameObjectWithTag("manager").GetComponent<scr_Manager>();
        hp = maxhp;
        rb = GetComponent<Rigidbody2D>();
        colision = transform.GetChild(0).GetChild(0);
        lolo = transform.localScale;
        lolostatic = lolo;
        if (lolo.x > 0)
            lolo.x = 1;
        else if (lolo.x < 0)
            lolo.x = -1;
    }

    // Update is called once per frame
    void Update()
    {
        //print(ani.GetInteger("StateMachine") + "-" + DoingAction + "--" + "lightcom: " + lightcomN + " -------:" + hp + "/" + maxhp);


        if (Input.GetKeyDown(KeyCode.UpArrow) && canjump)
        {
            jump = true;
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow) && plateffector)//Go down
        {
            colision.GetComponent<CapsuleCollider2D>().enabled = false;
            Invoke("comebackcollider", 0.7f);
            plateffector = false;
        }
        //print(ani.GetInteger("StateMachine") + "-" + lightcomN);

        //CORROUTINES--------------
        if (Input.GetKeyDown(KeyCode.K))
        {
            StartCoroutine(lightcom("f"));
        }
        else if (Input.GetKeyDown(KeyCode.L))
        {
            StartCoroutine(lightcom("g"));
        }


        if (Input.GetKeyDown(KeyCode.I))
        {
            StartCoroutine(magiccom("e"));
        }
        else if (Input.GetKeyDown(KeyCode.O))
        {
            StartCoroutine(magiccom("r"));
            StartCoroutine(magiccom2("e"));
        }
        else if (Input.GetKeyDown(KeyCode.P))
        {
            StartCoroutine(magiccom("t"));
            StartCoroutine(magiccom2("r"));
        }
        //CORROUTINES--------------

        //shield
        if (Input.GetKey(KeyCode.U))
        {
            colision.gameObject.layer = 11;
            shield = true;
        }
        else
        {
            colision.gameObject.layer = 9;
            shield = false;
        }
        shieldSprite.enabled = shield;

        //movement
        if (!DoingAction)
        {
            if (Input.GetKey(KeyCode.LeftArrow))
                xmove = -1;
            else if (Input.GetKey(KeyCode.RightArrow))
                xmove = 1;
            else
                xmove = 0;
            //xmove = Input.GetAxisRaw("Horizontal");

        }
        ymove = rb.velocity.y;
        statemachine();
        vision();
    }

    //State machine senzill per als diferents estats del personatge
    private void statemachine()
    {
        if (!DoingAction)
        {
            if (xmove == 0 && ymove == 0)
            {
                ani.SetInteger("StateMachine", 0);
            }
            else if ((xmove == -1 && lolo.x < 0 && ymove == 0) || (xmove == 1 && lolo.x > 0 && ymove == 0))
            {
                ani.SetInteger("StateMachine", 1);
            }
            else if ((xmove == -1 && lolo.x > 0 && ymove == 0) || (xmove == 1 && lolo.x < 0 && ymove == 0))
            {
                ani.SetInteger("StateMachine", 2);
            }
            if (ymove > 0)
            {
                ani.SetInteger("StateMachine", 5);
            }
            else if (ymove < 0)
            {
                ani.SetInteger("StateMachine", 6);
            }
        }
        else
        {
            ani.SetInteger("StateMachine", stanum);
        }/*
        if (test)
        {
            ani.SetInteger("StateMachine", 21);
        }*/
    }

    void FixedUpdate()
    {
        if (!DoingAction)
        {
            move(xmove, jump);
        }
        //RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector3.down, 0.3f);
        ////Debug.DrawRay(transform.position, Vector3.down,Color.red);

        //if (hit != null && hit.collider.tag == "flo")
        //{
        //    jumpcount = 2;
        //}
    }

    //Nomes called quan baixa del platform effector amb un invoke amb delay
    private void comebackcollider()
    {
        colision.GetComponent<CapsuleCollider2D>().enabled = true;
    }

    //Cambia on mira el personatge i el gira per a que estigui apuntant al oponent
    void vision()
    {
        if ((((gameObject.transform.position.x - adversary.transform.position.x) > 0) && lolo.x > 0) || (((gameObject.transform.position.x - adversary.transform.position.x) < 0) && lolo.x < 0))
        {
            lolo.x *= -1;
            lolostatic.x *= -1;
            transform.localScale = lolostatic;
            //farhit.GetComponent<AreaEffector2D>().forceAngle *=-1;

            if (!scalerX)
            {
                gameObject.transform.position = new Vector2(gameObject.transform.position.x - 0.75f, gameObject.transform.position.y);
                scalerX = true;
            }
            else
            {
                gameObject.transform.position = new Vector2(gameObject.transform.position.x + 0.75f, gameObject.transform.position.y);
                scalerX = false;
            }
        }



    }
    //Movement, valor jumpforce al salt per a poder cambiar-ho als diferents mapes
    private void move(float _xmove, bool _jump)
    {
        if (!conveyed)
            rb.velocity = new Vector2(spd * _xmove, rb.velocity.y);
        else if (xmove != 0)
            rb.velocity = new Vector2(spd * _xmove, rb.velocity.y);

        if (jump)
        {
            rb.velocity = new Vector2(rb.velocity.x, 0f);
            rb.AddForce(new Vector2(0f, jumpforce));
            canjump = false;
            jump = false;

        }

    }
    //CORROUTINES
    #region corrutines
    IEnumerator lightcom(string phase)
    {
        if (phase == "f")
        {
            if (lightcomN == 0 && (stanum != 24 || stanum != 23))
            {
                //lighthit.GetComponent<Collider2D>().enabled = true;
                DoingAction = true;
                lightcomN = 1;
                hitcounter = 0;
                hitbool = false;
                dmgval = 20;
                stanum = 21;
                //ani.SetInteger("StateMachine", 21);
                yield return new WaitForSeconds(0.917f);
                if (hitbool)
                    adversary.GetComponent<Reimu>().hit(dmgval);
                if (lightcomN == 1)
                {
                    ani.SetInteger("StateMachine", 0);
                    DoingAction = false;
                    lightcomN = 0;
                    //ani.SetInteger("StateMachine", 0);
                    //lighthit.GetComponent<Collider2D>().enabled = false;
                    //print("f com 1");
                }
            }
            else if (lightcomN == 2)
            {
                DoingAction = true;
                //if(!repeat)
                lightcomN = 3;
                hitcounter = 0;
                hitbool = false;
                dmgval = 30;
                stanum = 23;
                //print("soy 23 "+DoingAction);
                //ani.SetInteger("StateMachine", 21);
                //test = true;
                //print(lightcomN);
                yield return new WaitForSeconds(1f);
                //print(lightcomN);
                if (hitbool)
                    adversary.GetComponent<Reimu>().hit(dmgval);
                if (lightcomN == 3)
                {
                    //print("no he llegado al 24");
                    //test = false;
                    ani.SetInteger("StateMachine", 0);
                    DoingAction = false;
                    lightcomN = 0;
                }
                //repeat = false;
            }
            else if (lightcomN == 3)
            {
                DoingAction = true;
                //if(!repeat)
                lightcomN = 2;
                hitcounter = 0;
                hitbool = false;
                dmgval = 30;
                stanum = 24;
                //print("soy 24" + DoingAction);
                //ani.SetInteger("StateMachine", 21);
                //test = true;
                yield return new WaitForSeconds(1f);
                if (hitbool)
                    adversary.GetComponent<Reimu>().hit(dmgval);
                if (lightcomN == 2)
                {
                    //test = false;
                    //print("no he hecho repeat");
                    ani.SetInteger("StateMachine", 0);
                    DoingAction = false;
                    lightcomN = 0;
                }
                //repeat = false;
            }
        }
        else if (phase == "g")
        {
            if (lightcomN == 1)
            {
                DoingAction = true;
                //mist.SetActive(true);
                //lighthit.GetComponent<Collider2D>().enabled = false;
                //print("n2");
                lightcomN = 2;
                hitcounter = 0;
                hitbool = false;
                dmgval = 40;
                stanum = 22;

                //ani.SetInteger("StateMachine", 22);
                yield return new WaitForSeconds(0.917f);
                if (hitbool)
                    adversary.GetComponent<Reimu>().hit(dmgval);
                if (lightcomN == 2)
                {
                    //mist.SetActive(false);
                    ani.SetInteger("StateMachine", 0);
                    DoingAction = false;
                    //print("f com 2");
                    lightcomN = 0;
                }
            }
        }
        //DoingAction = false;
    }

    IEnumerator magiccom(string phase)
    {
        if (phase == "e")
        {
            if (magiccomN == 0)
            {
                magiccomN = 1;
                yield return new WaitForSeconds(0.5f);
                if (magiccomN == 1)
                {
                    magiccomN = 0;
                }
            }
        }
        else if (phase == "r")
        {
            if (magiccomN == 1)
            {
                magiccomN = 2;
                yield return new WaitForSeconds(0.5f);
                if (magiccomN == 2)
                {
                    magiccomN = 0;
                }
            }
        }
        else if (phase == "t")
        {
            DoingAction = true;
            if (magiccomN == 2)
            {
                //print("magicattack");
                magiccomN = 3;
                hitcounter = 0;
                hitbool = false;
                dmgval = 40f;
                stanum = 31;
                //spawnear projectile
                Quaternion rot;
                if (lolo.x > 0)
                    rot = Quaternion.Euler(new Vector3(0f, 0f, 65f));
                else
                    rot = Quaternion.Euler(new Vector3(0f, 0f, 115f));
                yield return new WaitForSeconds(0.2f);
                GameObject p = Instantiate(projectileprefab, new Vector2(this.transform.position.x + (0f * lolo.x), this.transform.position.y + 2f), rot);
                p.GetComponent<scrProjectile>().projmode = 1;
                yield return new WaitForSeconds(0.5f);
                ani.SetInteger("StateMachine", 0);
                DoingAction = false;
                magiccomN = 0;
            }
        }
        DoingAction = false;
    }

    IEnumerator magiccom2(string phase)
    {
        if (phase == "e")
        {
            if (magiccomN2 == 0)
            {
                magiccomN2 = 1;
                yield return new WaitForSeconds(0.5f);
                if (magiccomN2 == 1)
                {
                    magiccomN2 = 0;
                }
            }
        }
        else if (phase == "r")
        {
            if (magiccomN2 == 1)
            {
                magiccomN2 = 2;
                yield return new WaitForSeconds(0.5f);
                if (magiccomN2 == 2)
                {
                    magiccomN2 = 0;
                }
            }
            else if (magiccomN2 == 2)
            {
                DoingAction = true;
                print("magicattack");
                magiccomN2 = 3;
                hitbool = false;
                stanum = 31;
                //spawnear projectile
                Quaternion rot;
                if (lolo.x > 0)
                    rot = Quaternion.Euler(new Vector3(0f, 0f, 65f));
                else
                    rot = Quaternion.Euler(new Vector3(0f, 0f, 115f));
                yield return new WaitForSeconds(0.2f);
                GameObject p = Instantiate(projectileprefab, new Vector2(this.transform.position.x + (1f * lolo.x), this.transform.position.y + 1f), rot);
                p.GetComponent<scrProjectile>().target = adversary;
                p.GetComponent<scrProjectile>().projmode = 3;

                yield return new WaitForSeconds(0.5f);
                ani.SetInteger("StateMachine", 0);
                DoingAction = false;
                magiccomN2 = 0;
            }
        }
        DoingAction = false;
    }
    #endregion

    //COLLISION DETECTORS
    #region collision detectors
    private void OnCollisionEnter2D(Collision2D c)
    {
        if (c.transform.tag == "flo")
        {
            canjump = true;
        }
        if (c.transform.name == "plateffector")
        {
            plateffector = true;
        }
        //print(c.collider.name);
        if (c.transform.name == "conveyorbelt")
            conveyed = true;
    }


    private void OnCollisionExit2D(Collision2D c)
    {
        if (c.transform.name == "conveyorbelt")
            conveyed = false;
    }
    private void OnParticleCollision(GameObject other)
    {
        if (other.gameObject.tag == "player2")
        {
            print("particlehit");
        }
    }
    private void OnTriggerEnter2D(Collider2D c)
    {
        if (c.transform.tag == "flo")
        {

            canjump = true;
        }
        if (c.tag == "player1")
        {
            if (!hitbool)
                hitbool = true;
            hitcounter++;
            if (gameObject.transform.position.x - c.gameObject.transform.position.x < 0)
            {
                farhit.GetComponent<AreaEffector2D>().forceAngle = 55;
                //c.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(80f,120f));
            }
            else
            {
                farhit.GetComponent<AreaEffector2D>().forceAngle = 125;
                //c.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(-80f, 120f));
            }

            //forcepush
            //farhit.GetComponent<AreaEffector2D>().forceMagnitude += hitcounter;

            /*if (mist.activeSelf)
            {
                hp--;
            }*/
            farhit.GetComponent<CircleCollider2D>().enabled = false;
            midhit.GetComponent<CircleCollider2D>().enabled = false;
            closehit.GetComponent<CircleCollider2D>().enabled = false;
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.transform.tag == "dmgarea")
        {
            hit(0.2f);
        }
    }

    #endregion
    public float getHP()
    {
        return hp;
    }

    //invoke del delegat per a actualitzar la vida
    public void hit(float attackdmg)
    {
        if (!shield)
        {
            hp -= attackdmg;
            manager.GetHpchange().Invoke();
            bloodemitter.GetComponent<ParticleSystem>().Play();
        }

    }
}
