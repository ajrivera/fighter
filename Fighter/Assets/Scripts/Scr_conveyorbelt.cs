﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_conveyorbelt : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("changedir", 0f, 4f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void changedir()
    {
        GetComponent<SurfaceEffector2D>().speed *= -1;
    }
}
