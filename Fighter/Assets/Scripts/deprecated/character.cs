using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class character : MonoBehaviour
{
    private float spd = 3.5f;
    private Rigidbody2D charrb;

    private float maxhp = 240;
    private float hp = 0;


    private float xmove = 0f;
    private bool jump = false;
    private bool canjump = false;
    private bool visright = true;

    public GameObject adversary;

    public Animator ani;

    public GameObject lighthit;
    public GameObject hardhit;
    public GameObject lighthitup;
    
    int lightcomN = 0;
    Vector3 lolo;

    void Start()
    {
        hp = maxhp;
        charrb = GetComponent<Rigidbody2D>();
        lolo = transform.localScale;
    }

    // Update is called once per frame
    void Update()
    {

        xmove = Input.GetAxisRaw("Horizontal");
        if (Input.GetKeyDown(KeyCode.W) && canjump)
        {
            jump = true;
        }
        if (Input.GetKeyDown(KeyCode.F))
        {
            StartCoroutine(lightcom("f"));
        }
        else if (Input.GetKeyDown(KeyCode.G))
        {
            StartCoroutine(lightcom("g"));
        }
        vision();
    }

    
    void FixedUpdate()
    {
        move(xmove, jump);
        //RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector3.down, 0.3f);
        ////Debug.DrawRay(transform.position, Vector3.down,Color.red);

        //if (hit != null && hit.collider.tag == "flo")
        //{
        //    jumpcount = 2;
        //}
    }


    void vision()
    {
        if ((((gameObject.transform.position.x - adversary.transform.position.x) > 0) && lolo.x > 0) || (((gameObject.transform.position.x - adversary.transform.position.x) < 0) && lolo.x < 0))
        {
            lolo.x *= -1;
        }
        transform.localScale = lolo;
    }
    private void move(float _xmove, bool _jump)
    {

        charrb.velocity = new Vector2(spd * _xmove, charrb.velocity.y);

        if (jump)
        {
            charrb.velocity = new Vector2(charrb.velocity.x, 0f);
            charrb.AddForce(new Vector2(0f, 200f));
            canjump = false;
            jump = false;

        }

    }
    IEnumerator lightcom(string phase)
    {
        if (phase == "f")
        {
            if (lightcomN == 0)
            {
                //lighthit.GetComponent<Collider2D>().enabled = true;
                print("n1");
                lightcomN = 1;
                ani.SetInteger("StateMachine", 1);
                yield return new WaitForSeconds(0.5f);
                if (lightcomN == 1)
                {
                    ani.SetInteger("StateMachine", 0);
                    //lighthit.GetComponent<Collider2D>().enabled = false;
                    print("f com 1");
                    lightcomN = 0;
                }
            }
        }
        else if (phase == "g")
        {
            if (lightcomN == 1)
            {
                ani.SetInteger("StateMachine", 0);
                //lighthit.GetComponent<Collider2D>().enabled = false;
                hardhit.GetComponent<Collider2D>().enabled = true;
                print("n2");
                lightcomN = 2;
                yield return new WaitForSeconds(0.8f);
                if (lightcomN == 2)
                {
                    hardhit.GetComponent<Collider2D>().enabled = false;
                    print("f com 2");
                    lightcomN = 0;
                }
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D c)
    {
        if (c.transform.tag == "flo")
        {
            canjump = true;
        }

    }
    private void OnTriggerEnter2D(Collider2D c)
    {
        if (c.tag == "enemy")
        {
            if (gameObject.transform.position.x - c.gameObject.transform.position.x < 0)
            {
                c.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(80f,120f));
            }else{
               c.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(-80f, 120f));
            }
        }
    }

    public float getHP()
    {
        return hp;
    }
    public void setHP(float newhp)
    {
        hp = newhp;
    }
}
