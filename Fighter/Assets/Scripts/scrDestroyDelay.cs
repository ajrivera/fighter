﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scrDestroyDelay : MonoBehaviour
{
    public float delay;
    //explosion
    public bool insta = false;
    public GameObject particle;
    public bool force = false;
    public float forcepush;

    //players
    private GameObject player1;
    private GameObject player2;
    void Start()
    {
        //getplayers
        player1 = GameObject.FindGameObjectWithTag("player1");
        player2 = GameObject.FindGameObjectWithTag("player2");
    }

    // Update is called once per frame
    void Update()
    {
        Invoke("inst", delay);
    }

    private void inst()
    {
        //instantiate explosion
        if (insta)
        {
            GameObject part = Instantiate(particle);
            part.transform.position = transform.position;
            part.transform.SetParent(null);
        }
        Destroy(gameObject);
    }

    //hit players and push them
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform.tag == "player2" && force)
        {
            player2.GetComponentInParent<Marisa>().hit(10f);
            Vector3 pushvec = other.transform.position - transform.position;
            pushvec.Normalize();

            other.GetComponentInParent<Rigidbody2D>().AddForce(new Vector2(pushvec.x, pushvec.y+1f) * forcepush, ForceMode2D.Impulse);

            //other.GetComponent<Rigidbody2D>().AddForce(new Vector2(pushvec.x, pushvec.y) * forcepush, ForceMode2D.Impulse);

            GetComponent<CircleCollider2D>().enabled = false;
        }
        else if (other.transform.tag == "player1" && force)
        {
            player1.GetComponentInParent<Reimu>().hit(10f);
            Vector3 pushvec = other.transform.position - transform.position;
            pushvec.Normalize();

            other.GetComponentInParent<Rigidbody2D>().AddForce(new Vector2(pushvec.x, pushvec.y+1f) * forcepush, ForceMode2D.Impulse);

            //other.GetComponent<Rigidbody2D>().AddForce(new Vector2(pushvec.x, pushvec.y) * forcepush, ForceMode2D.Impulse);

            GetComponent<CircleCollider2D>().enabled = false;
        }

    }
}

