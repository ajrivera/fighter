﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class scrProjectile : MonoBehaviour
{
    //proj vars
    private float projspd;
    private float projdmg;
    public float floheight;
    public int projmode;        
    //modes:
        //  0 = proyectil que rebota y deja areas de daño (Limite rebotes)
        //  1 = proyectiles radiales en multiples direcciones (Limite rebotes)
        //  2 = proyectiles simples forward (instanciados en el 1)
        //  3 = proyectil dirigido
    private Rigidbody2D rb;

    public int projlife = 1;
    //num proj in mode 1
    public int case2shotnumber = 4;

    //gameobjects
    public GameObject areadmg;
    public GameObject particleexp;
    public GameObject case2shot;
    public GameObject case3shotTrail;

    //oponent
    public GameObject target;
    public float rotsmth = 0.5f;

    //players
    public GameObject player1;
    public GameObject player2;

    // Start is called before the first frame update
    void Start()
    {
        //get players
        player1 = GameObject.FindGameObjectWithTag("player1");
        player2 = GameObject.FindGameObjectWithTag("player2");
        rb = GetComponent<Rigidbody2D>();
        switch (projmode)
        {
            case 0: //set de gravedad i impulso inicial
                rb.gravityScale = 3f;
                projspd = 14f;
                if (SceneManager.GetActiveScene().name == "Map 02" || SceneManager.GetActiveScene().name == "Map 03")
                    floheight = player1.transform.position.y;
                rb.AddRelativeForce(new Vector2(projspd, 0f), ForceMode2D.Impulse);
                break;
            case 1: //desactivamos collider para instanciacion de proyectiles
                GetComponent<CircleCollider2D>().enabled = false;

                projspd = 12f;

                //calculamos los angulos de disparo dependiente del numero de proyectiles i instanciamos
                float angle = 360 / case2shotnumber;
                for (int i = 1; i < case2shotnumber + 1; i++)
                {
                    float angleaux = angle * i;
                    Quaternion rot = Quaternion.Euler(0f, 0f, angleaux);
                    GameObject shot = Instantiate(case2shot, transform.position, rot);
                    //aplicamos desplazamiento en direccion x local para evitar que colisionen entre ellos
                    shot.transform.Translate(3f, 0f, 0f);
                    shot.transform.SetParent(null);
                }
                // Destruimos el objeto emisor
                Destroy(gameObject);
                break;
            case 2: //set de gravedad i impulso inicial

                rb.gravityScale = 0f;
                projspd = 8f;
                rb.AddRelativeForce(new Vector2(projspd, 0f), ForceMode2D.Impulse);
                break;
            case 3: //set de gravedad, activar vida del proyectil, activar efecto trail
                projspd = 4f;
                rb.gravityScale = 0f;
                GetComponent<scrDestroyDelay>().enabled = true;
                GetComponent<scrDestroyDelay>().forcepush = 5f;
                case3shotTrail.SetActive(true);
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    void FixedUpdate()
    {
        switch (projmode)
        {
            case 0:
                break;
            case 1:
                break;
            case 2:
                break;
            case 3:
                //rotar hacia el objetivo
                Vector3 diff = target.transform.position - transform.position;
                diff.Normalize();
                float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;

                //aplicar lerp de rotacion
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0f, 0f, rot_z), rotsmth);

                //aplicar velocidad actualizada segun nueva rotacion
                rb.velocity = transform.TransformDirection(new Vector2(projspd, 0f));
                break;
        }
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        switch (projmode)
        {
            case 0://rebota y instancia areadmg
                if (col.transform.tag == "flo")
                {
                    floheight = this.transform.position.y;
                    GameObject areadmginst = Instantiate(areadmg);
                    areadmginst.GetComponent<scrDestroyDelay>().force = false;
                    areadmginst.transform.SetParent(null);
                    areadmginst.transform.position = new Vector3(transform.position.x, floheight, transform.position.z);
                    projlife--;
                    if (projlife <= 0)
                    {
                        GameObject part = Instantiate(particleexp);
                        part.transform.SetParent(null);
                        part.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);
                        Destroy(this.gameObject);
                    }
                }//si pega al player 2 hace dmg
                else if (col.transform.tag == "player2")
                {
                    //print("hit");
                    player2.GetComponent<Marisa>().hit(20f);
                }
                break;
            case 1://se destruye antes de que pueda colisionar, (Solo se utiliza para instanciar las del mode 2)
                break;
            case 2://rebotan un num(projlife) hasta ser destruidas o rebotar con el player 1
                projlife--;
                if (projlife <= 0)
                {
                    GameObject part = Instantiate(particleexp);
                    part.transform.SetParent(null);
                    part.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);
                    Destroy(this.gameObject);
                }
                if (col.transform.tag == "player1")
                {
                    player1.GetComponent<Reimu>().hit(15f);
                }
                break;
            case 3://if hit quita vida;
                if (col.transform.tag == "player1")
                {
                    player1.GetComponent<Reimu>().hit(15f);
                }
                else if (col.transform.tag == "player2")
                {
                    player2.GetComponent<Marisa>().hit(15f);
                }
                //instancia particle explosion
                GameObject part2 = Instantiate(particleexp);
                part2.transform.SetParent(null);
                part2.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);
                Destroy(this.gameObject);
                break;
        }
    }
}
