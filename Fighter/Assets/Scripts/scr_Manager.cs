﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class scr_Manager : MonoBehaviour
{
    //players
    public GameObject p1;
    public GameObject p2;
    //public character p2;

    //hpbars
    public Image hpbar;
    public Image hpbar2;
    //event
    public delegate void hpchange();
    public event hpchange hpbrr;

    public AudioSource ost;

    //scenename
    private string scenename;

    // Start is called before the first frame update
    void Start()
    {
        ost.loop = true;
        ost.Play();
        scenename = SceneManager.GetActiveScene().name;
        switch (scenename)
        {
            //change jump according to map
            case "Map 01":
                p1.GetComponent<Reimu>().jumpforce = 400f;
                p2.GetComponent<Marisa>().jumpforce = 400f;
                break;
            case "Map 02":
                p1.GetComponent<Reimu>().jumpforce = 450f;
                p2.GetComponent<Marisa>().jumpforce = 450f;
                break;
            case "Map 03":
                p1.GetComponent<Reimu>().jumpforce = 400f;
                p2.GetComponent<Marisa>().jumpforce = 400f;

                break;
            default:
                p1.GetComponent<Reimu>().jumpforce = 400f;
                p2.GetComponent<Marisa>().jumpforce = 400f;
                break;
        }
        hpbrr += minhp;
    }

    // Update is called once per frame
    void Update()
    {
        //if dead mapchooser = true
        if (p1.GetComponent<Reimu>().getHP() <= 0 || p2.GetComponent<Marisa>().getHP() <= 0)
        {
            SceneManager.LoadScene("Start");
        }
        //debug
        /*if (p2 != null)
        {
            if (p2.transform.position.x < -8 || p2.transform.position.x > 8)
            {
                p2.transform.position = new Vector2(0f, 2f);
            }
        }*/
    }

    //event getter, maybe useless
    public hpchange GetHpchange()
    {
        return hpbrr;
    }


    //update hpbars
    public void minhp()
    {
        hpbar.GetComponent<RectTransform>().sizeDelta = new Vector2(p1.GetComponent<Reimu>().getHP(), hpbar.GetComponent<RectTransform>().sizeDelta.y);
        hpbar2.GetComponent<RectTransform>().sizeDelta = new Vector2(p2.GetComponent<Marisa>().getHP(), hpbar2.GetComponent<RectTransform>().sizeDelta.y);
        //hpbar.GetComponent<RectTransform>().sizeDelta = new Vector2(p2.getHP(), hpbar.GetComponent<RectTransform>().rect.height);
    }

}
