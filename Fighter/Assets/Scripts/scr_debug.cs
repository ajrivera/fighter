﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_debug : MonoBehaviour
{
    private float maxhp = 240;
    private float hp = 240;

    private scr_Manager manager;
    private bool shield = false;
    // Start is called before the first frame update
    void Start()
    {
        manager = GameObject.FindGameObjectWithTag("manager").GetComponent<scr_Manager>();
        hp = maxhp;
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        //print(collision.transform.name);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //print(collision.transform.name);
    }
    public float getHP()
    {
        return hp;
    }
    public void hit(float attackdmg)
    {
        if (!shield)
        {
            hp -= attackdmg;
            manager.GetHpchange().Invoke();

        }
    }
}
