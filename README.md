# Inicio
Juego Fighter de temàtica Anime basado en personajes de Touhou.
No me eches mucho hate por el animator :(.

# Controles Player 1: Reimu

| Teclas | Acción |
| ------ | ------ |
| A/D | `Moviment Horizontal` |
| W | `Jump` |
| S | `Bajar Plataforma` |
| Q | `Shield` |
| E->R->R | `Proyectil 3 (Persigue)` |
| E->R->T | `Proyectil 1 (Rebota y deja area)` |
| F | `Attack 1` |
| F->G | `Attack 2` |
| F->G->F | `Attack 3` |
| F->G->F ... F->F | `Attack 3 (Puede loopear entre si)` |

# Controles Player 2: Marisa

| Teclas | Acción |
| ------ | ------ |
| ←/→ | `Moviment Horizontal` |
| ↑ | `Jump` |
| ↓ | `Bajar Plataforma` |
| U | `Shield` |
| O->P->P | `Proyectil 3 (Persigue)` |
| I->O->P | `Proyectil 2 (BulletHell)` |
| K | `Attack 1` |
| K->L | `Attack 2` |
| K->L->K | `Attack 3` |
| K->L->K ... K->K | `Attack 3 (Puede loopear entre si)` |

# Requisitos Mínimos

Dues escenes de combat (han de tenir particularitats diferencials, com plataformes, obstacles, lava, fricció,  etc) `[Mapa 02 plat effector, Mapa 03 buoyancy, conveyor belt]`

Han d’haver dos jugadors com a mínim. Els dos jugadors han de ser diferents i incorporar:
- Hitbox i Hurtbox `[Hit triggers, Hurt Colision]`
- Barra de vida o similar (que es tindrà actualitzada per delegats)
- Les animacions han de moure les hitboxes i hurtboxes corresponents `[Frame by Frame]`
- Les colisions tenen que estar correctament implementades, amb opció de bloqueig. `[El bloqueig cambia la colision a un layer invencible]`
- Els impactes han de tenir reaccions físiques (haura de sortir rebutjat, o recular, o caure) `[Fet amb effector]`
- Possibles combos, controlats per temps amb corrutines 
- Projectils (amb físiques complexes, que no vagin en linea recta) 
- Efectes de particles `[Proyectils,Sang dels Players, ns si algo más]`

# Ampliaciones
- Animator amb Frame by Frame `[Totes les animacions...]`
- Efectes físics de més complexitat `[Us de materials amb Bounciness,Friction i effectors]`
- Efectes visuals extres més enllà d’efects de partícles bàsics `[Iluminacio a tots els mapes, Particles més complexes als Proyectils]`
- Efectes d’audio i vídeo `[Musica de fons]`
- Ús d’Effectors `[Mapa 02 Platform effector one way, Mapa 03 Buoyancy(rebota el personatge una mica per la densitat) i Surface(cambia la direcció cada 4 segons)]`